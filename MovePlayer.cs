﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour {
  
    public float speed = 10; // Can be changed in Unity Inspector. Default 10
    public float jumpPower = 10; // Can be changed in Unity Inspector. Default 10
    
    private Rigidbody playerRB;

    void Start()
    {
        playerRB = gameObject.GetComponent<Rigidbody>(); //Get the attached GameObjects rigidbody (Typically the players)
    }

	void FixedUpdate () {
        /* Get which buttons are being pressed */
        float moveX = Input.GetAxis("Horizontal");  // any number from -1 to 1      -1, -0.99, ect
        float moveZ = Input.GetAxis("Vertical");
        float moveY = Input.GetAxisRaw("Jump"); // Raw means -1 or 0 or 1

        /* If they not moving up or down allow them to move and jump */
        if (Mathf.Abs(playerRB.velocity.y) <= 0.001)
        {
            Vector3 movement = new Vector3(moveX, moveY * jumpPower, moveZ);
            playerRB.AddForce(movement * speed);
        }

    }
}
