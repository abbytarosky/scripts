﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    public Transform player;  // Drag and drop the player object onto this in the Unity Inspector

    private Vector3 offset;
    private Transform myCamera;

    void Start()
    {
        myCamera = gameObject.transform;
        offset = myCamera.position - player.position;
    }

    void Update()
    {
        myCamera.position = player.position + offset;
    }
}
