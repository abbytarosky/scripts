﻿using UnityEngine;
using System.Collections;

public class MazeMaker : MonoBehaviour {
    
    public int numberOfCoins = 20;
    public int smoothTimes = 2;
    public Vector2 groundSize = new Vector2(1000,1000);
    public GameObject ground;
    public GameObject collectables;

    [Range(1,9)]
    public int smoothing;

    [Range(1, 100)]
    public int numberOfHoles = 10;


    private int[,] groundDisplay;

    void Start()
    {
        groundDisplay = new int[(int)groundSize.x, (int)groundSize.y];
        for (int i = 0; i < groundSize.x; i++)
        {
            for (int j = 0; j < groundSize.y; j++)
            {
                groundDisplay[i, j] = 1;
            }
        }
        
        removePieces();
        for(int i = 0; i < smoothTimes; i ++)
            smoothPieces();
        placeCoins();
        displayBoard();

    }

    void removePieces()
    {
        for(int i = 0; i < numberOfHoles/100.0 * groundSize.x * groundSize.y; i ++)
        {
            int xPos = (int)Random.Range(1, groundSize.x - 1);
            int yPos = (int)Random.Range(1, groundSize.y - 1);
           

            while(groundDisplay[xPos, yPos] == 0 || (groundDisplay[xPos-1, yPos] == 0 
                && groundDisplay[xPos + 1, yPos] == 0 
                && groundDisplay[xPos, yPos-1] == 0 
                && groundDisplay[xPos, yPos + 1] == 0) )
            {
                xPos = (int)Random.Range(1, groundSize.x - 1);
                yPos = (int)Random.Range(1 , groundSize.y - 1);
            }

            groundDisplay[xPos, yPos] = 0;
        }
    }

    void smoothPieces()
    {
        int total;
        for (int i = 1; i < groundSize.x  - 1; i++)
        {
            for (int j = 1; j < groundSize.y - 1; j++)
            {
                total = 0;
                total += groundDisplay[i + 1, j];
                total += groundDisplay[i - 1, j];
                total += groundDisplay[i, j + 1];
                total += groundDisplay[i, j - 1];
                total += groundDisplay[i + 1, j + 1];
                total += groundDisplay[i + 1, j - 1];
                total += groundDisplay[i - 1, j + 1];
                total += groundDisplay[i - 1, j - 1];
                if (total <= smoothing)
                    groundDisplay[i, j] = 0;
                else
                    groundDisplay[i, j] = 1;
            }
        }
    }

    void placeCoins()
    {
        for(int i = 0; i < numberOfCoins; i++)
        {
            int xPos = Random.Range(0, (int)groundSize.x);
            int yPos = Random.Range(0, (int)groundSize.y);

            while(groundDisplay[xPos, yPos] == 0)
            {
                xPos = Random.Range(0, (int)groundSize.x);
                yPos = Random.Range(0, (int)groundSize.y);
            }

            Instantiate(collectables, new Vector3(
                xPos * ground.GetComponent<Renderer>().bounds.size.x, 
                1, 
                yPos * ground.GetComponent<Renderer>().bounds.size.z), Quaternion.identity);
        }
    }

    void displayBoard()
    {
        for (int i = 0; i < groundSize.x; i++)
        {
            for (int j = 0; j < groundSize.y; j++)
            {
                if(groundDisplay[i,j] == 1)
                    Instantiate(ground, new Vector3(
                        i * ground.GetComponent<Renderer>().bounds.size.x, 
                        -ground.GetComponent<Renderer>().bounds.size.y/2, 
                        j * ground.GetComponent<Renderer>().bounds.size.z), Quaternion.identity);
            }
        }
    }

}
