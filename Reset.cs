﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Reset : MonoBehaviour {

	void Update () {
        //If there are no objects tagged "Coin" left, restart the level *or what you will*
        if (GameObject.FindGameObjectWithTag("Coin") == null)
            SceneManager.LoadScene("Level1");


        //If they player is beyond the bounds of the game, they lose. Restart
        if(GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>().position.y < -20)
            SceneManager.LoadScene("Level1");

	}

}
