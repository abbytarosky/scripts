﻿using UnityEngine;
using System.Collections;

public class Spin : MonoBehaviour {

    public float speed;

	void Update () {
        gameObject.transform.Rotate(new Vector3(speed * Time.deltaTime, -speed * Time.deltaTime, speed * Time.deltaTime));
	}
}
